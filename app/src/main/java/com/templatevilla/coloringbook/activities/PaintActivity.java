package com.templatevilla.coloringbook.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PointF;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.templatevilla.coloringbook.adapter.DialogAdapter;
import com.templatevilla.coloringbook.adapter.SubColorAdapter;
import com.templatevilla.coloringbook.colorManager.ColorUtil;
import com.templatevilla.coloringbook.colorManager.GetColors;
import com.templatevilla.coloringbook.constant.Constant;
import com.templatevilla.coloringbook.utils.ModelImagePoint;
import com.templatevilla.coloringbook.utils.TheTask;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.madrapps.pikolo.HSLColorPicker;
import com.madrapps.pikolo.listeners.SimpleColorSelectionListener;
import com.templatevilla.coloringbook.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.templatevilla.coloringbook.constant.Constant.IMG_FOLDER;
import static com.templatevilla.coloringbook.constant.Constant.imgSavedOrNot;

/**
 * Created by templatesvilla on 7/30/2018.
 */

public class PaintActivity extends AppCompatActivity implements SubColorAdapter.ClickInterface {


    public static int replace_color, fill_color;
    public static String selected_color;
    public static Bitmap bitmap;
    public static Bitmap originalBitmap;
    public static Bitmap currentBitmap;
    public static List<ModelImagePoint> undolist;
    private static int currentX;
    private static int currentY;
    @BindView(R.id.btn_close)
    TextView btn_close;
    @BindView(R.id.image_draw)
    TouchImageViewNew img_draw;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.pager_rec)
    RecyclerView pager_rec;
    @BindView(R.id.rec_dialog)
    RecyclerView rec_dialog;
    @BindView(R.id.pager_color)
    ViewPager pager_color;
    @BindView(R.id.layout_dialog)
    RelativeLayout layout_dialog;
    @BindView(R.id.btn_brush)
    ImageView btn_brush;
    @BindView(R.id.btn_colorpicker)
    ImageView btn_colorpicker;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.layout_space)
    LinearLayout layout_space;
    @BindView(R.id.layout_paint)
    LinearLayout layout_paint;
    @BindView(R.id.layout_ad)
    RelativeLayout layout_ad;
    int getcolor;
    boolean isSave;
    int getpos;
    RecyclerView rec_2;
    ColorPagerAdapter colorPagerAdapter;
    SubColorAdapter pagerSubRec;
    DialogAdapter dialogRecAdapter;
    Point point;
    Dialog dialog;
    HSLColorPicker dialog_color_picker;
    Button dialog_btn_cancel, dialog_btn_ok;
    String imagepath;
    List<ColorUtil> colorslist = new ArrayList<>();
    AdView mAdView;
    boolean isEdit = false;
    List<String> imglist = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paint);
        ButterKnife.bind(this);
        selected_color = "null";
        getpos = getIntent().getIntExtra("pos", 0);

        if (getIntent().getStringExtra("imgPath") != null) {
            isEdit = true;
            imagepath = getIntent().getStringExtra("imgPath");
            Log.e("imgpath==", "" + imagepath);
        }
        Log.e("getpos1==", "" + getpos);
        fill_color = Color.parseColor("#F02640");
        selected_color = "#F02640";
        undolist = new ArrayList<>();
        init();
        showbanner();
        clickListener();

        dialog = new Dialog(PaintActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        setAdapters();
        SubColorAdapter.setInterface(this);

    }

    //  Method for show banner
    private void showbanner() {
        mAdView = findViewById(R.id.nativeadView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.paint_menu, menu);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem registrar = menu.findItem(R.id.iv_recreate);
        if (isEdit == true) {
            registrar.setVisible(false);
        } else {
            registrar.setVisible(true);
        }


        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.iv_done:
                SaveImage();
                selected_color = "null";
                Log.e("imgpathpaint==", "" + imagepath);
                try {
                    Intent intent = new Intent(getApplicationContext(), CreationActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                    Log.e("outmemory==", "" + e.getMessage());
                }

                break;
            case R.id.iv_recreate:
                recreateActivity();
                break;
            case R.id.iv_share:
                undo();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void undo() {

        Log.e("size1==", "" + undolist.size());

        if (undolist.size() > 0) {
            Log.e("size2==", "" + (undolist.size() - 1));
            new TheTask(bitmap, undolist.get(undolist.size() - 1).point, undolist.get(undolist.size() - 1).color, Color.WHITE, img_draw).execute(new Void[0]);
            undolist.remove(undolist.size() - 1);
        } else {
            Toast.makeText(this, "Clear Image", Toast.LENGTH_SHORT).show();

        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void clickListener() {

        btn_colorpicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layout_dialog.setVisibility(View.VISIBLE);
            }
        });
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layout_dialog.setVisibility(View.GONE);
            }
        });

        btn_brush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setColorDialog();
            }
        });
        img_draw.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP:
                        currentX = (int) event.getX();
                        currentY = (int) event.getY();

                        float devVsImgRatio = img_draw.drawableWidthForDeviceRelated / originalBitmap.getWidth();
                        PointF point = img_draw.transformCoordTouchToBitmap(event.getX(), event.getY(), true);
                        currentX = (int) (point.x / devVsImgRatio);
                        currentY = (int) (point.y / devVsImgRatio);
                        bitmap = currentBitmap;

                        int i = replace_color;
                        int j = fill_color;
                        Point point1 = new Point();
                        point1.x = currentX;
                        point1.y = currentY;
                        new TheTask(bitmap, point1, i, j, img_draw).execute(new Void[0]);
                        undolist.add(new ModelImagePoint(j, point1));

                        break;
                }
                return true;
            }
        });
    }


    private void setAdapters() {
        colorslist.clear();
        colorslist = GetColors.getallcolordata();
        colorPagerAdapter = new ColorPagerAdapter(colorslist, getApplicationContext());
        pager_color.setAdapter(colorPagerAdapter);
        dialogRecAdapter = new DialogAdapter(getApplicationContext(), colorslist, new DialogAdapter.ClickInterface() {
            @Override
            public void recItemClick(View view, int i) {
                pager_color.setCurrentItem(i);
                layout_dialog.setVisibility(View.GONE);
            }
        });
        rec_dialog.setAdapter(dialogRecAdapter);
        colorPagerAdapter.notifyDataSetChanged();
        dialogRecAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setAdapters();

    }

    private void init() {
        point = new Point();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setMainIntent();
            }
        });
        rec_dialog.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
        img_draw.setMaxZoom(15);
        imglist = Constant.getAllImages(IMG_FOLDER, getApplicationContext());
        if (imgSavedOrNot(imagepath)) {
            originalBitmap = BitmapFactory.decodeFile(imagepath);
        } else {
            try {
                originalBitmap = getBitmapFromAsset(IMG_FOLDER + "/" + imglist.get(getpos));
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
            }
        }

        img_draw.setImageBitmap(originalBitmap);

        try {
            currentBitmap = originalBitmap.copy(originalBitmap.getConfig(), true);
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onDestroy() {
        Runtime.getRuntime().gc();
        super.onDestroy();
    }

    private Bitmap getBitmapFromAsset(String strName) {
        AssetManager assetManager = getAssets();
        InputStream istr = null;
        try {
            istr = assetManager.open(strName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Bitmap bitmap = BitmapFactory.decodeStream(istr);
        return bitmap;
    }


    public void recreateActivity() {
        try {
            Intent intent = new Intent(getApplicationContext(), PaintActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            if (imagepath != null) {
                intent.putExtra("imgpath", imagepath);
            } else {
                intent.putExtra("pos", getpos);
            }
            startActivity(intent);
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            Log.e("outmemory==", "" + e.getMessage());
            finish();
        }
    }

    public void setColorDialog() {
        dialog.setContentView(R.layout.activity_colorpicker_dialog);
        dialog_color_picker = dialog.findViewById(R.id.dialog_color_picker);
        dialog_btn_cancel = dialog.findViewById(R.id.dialog_btn_cancel);
        dialog_btn_ok = dialog.findViewById(R.id.dialog_btn_ok);
        dialog_color_picker.setColorSelectionListener(new SimpleColorSelectionListener() {
            @Override
            public void onColorSelected(int color) {
                getcolor = color;
            }
        });

        dialog_btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog_btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selected_color = String.valueOf(getcolor);
                fill_color = getcolor;
                pager_color.setAdapter(colorPagerAdapter);
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    private void SaveImage() {

        img_draw.resetZoom();
        isSave = true;
        File dir = new File(Constant.SAVED_IMG_PATH);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        Bitmap res = Bitmap.createBitmap(img_draw.getWidth(), img_draw.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(res);
        img_draw.draw(canvas);

        File saveFile1;


        if (imagepath != null) {
            saveFile1 = new File(imagepath);
        } else {
            saveFile1 = new File(dir, System.currentTimeMillis() + ".jpg");
        }
        imagepath = String.valueOf(saveFile1);
        if (saveFile1.exists()) {
            saveFile1.delete();

        }

        try {

            FileOutputStream out = new FileOutputStream(saveFile1);
//            c.drawBitmap(bmp, 0, 0, null);
            res.compress(Bitmap.CompressFormat.PNG, 100, out);

            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Intent mediaScanIntent = new Intent(
                    Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri contentUri = Uri.parse("file://"
                    + Environment.getExternalStorageDirectory());
            mediaScanIntent.setData(contentUri);
            (PaintActivity.this).sendBroadcast(mediaScanIntent);
        } else {
            (PaintActivity.this).sendBroadcast(new Intent(
                    Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://"
                    + Environment.getExternalStorageDirectory())));
        }
        sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(saveFile1.getAbsolutePath()))));

        Toast.makeText(this, "saved successfully", Toast.LENGTH_SHORT).show();

    }


    // Item Click
    @Override
    public void ItemClick(View view, int i, String s, int pagerPosition) {
        selected_color = s;
        fill_color = Color.parseColor(selected_color);
        pager_color.setAdapter(colorPagerAdapter);
        pager_color.setCurrentItem(pagerPosition);
    }

    @Override
    public void onBackPressed() {
        if (!isSave) {
            showExitDialog();
        } else {
            finish();
        }
    }

    // Exit Page Dialog
    public void showExitDialog() {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(PaintActivity.this, R.style.MyDialogTheme);
        builder.setMessage("Are you sure you want to exit?");
        builder.setCancelable(false);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                setMainIntent();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        android.support.v7.app.AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    // Pass Intent
    public void setMainIntent() {
        try {
            selected_color = "null";
            if (isEdit == false) {
                Intent intent = new Intent(getApplicationContext(), SelectImageActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.putExtra("imgPosition", getpos);
                startActivity(intent);
            } else {
                Intent intent = new Intent(getApplicationContext(), CreationActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                //            try {
                //                finishAffinity();
                //            } catch (Exception e) {
                //                e.printStackTrace();
                //                finish();
                //            }
            }
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            Log.e("outmemory==", "" + e.getMessage());
            finish();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        checkOrientation(newConfig);

    }

    private void checkOrientation(Configuration newConfig) {
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Log.e("orientation===", "landscape");
            layout_ad.setVisibility(View.GONE);
            layout_space.setVisibility(View.GONE);
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            Log.e("orientation===", "portrait");
            layout_ad.setVisibility(View.VISIBLE);
            layout_space.setVisibility(View.VISIBLE);
        }
    }

    // Color Adapter
    public class ColorPagerAdapter extends PagerAdapter {
        List<ColorUtil> colorUtilslist;
        Context context;

        ColorPagerAdapter(List<ColorUtil> colorUtilslist, Context context) {
            this.colorUtilslist = colorUtilslist;
            this.context = context;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_pagercolor, container, false);
            TextView tv_color_name = view.findViewById(R.id.tv_color_name);
            rec_2 = view.findViewById(R.id.rec_2);
            tv_color_name.setText(colorUtilslist.get(position).name);
            pagerSubRec = new SubColorAdapter(getApplicationContext(), colorUtilslist.get(position).getSetColors(), position);
            rec_2.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
            rec_2.setAdapter(pagerSubRec);

            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return colorUtilslist.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

    }

}