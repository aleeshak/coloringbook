package com.templatevilla.coloringbook.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.templatevilla.coloringbook.adapter.CreationAdapter;
import com.templatevilla.coloringbook.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.templatevilla.coloringbook.constant.Constant.getAllCreationList;

/**
 * Created by Templatesvilla on 7/30/2018.
 *
 */

public class CreationActivity extends AppCompatActivity implements CreationAdapter.RecClickInterface {

    @BindView(R.id.rec_my_creation)
    RecyclerView rec_my_creation;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    List<String> strings;
    CreationAdapter myCreationAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_creation);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("My Creation");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        rec_my_creation.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
        strings = new ArrayList<>();
        strings = getAllCreationList();
        if (strings.isEmpty()) {

            Toast.makeText(this, "No creation found", Toast.LENGTH_SHORT).show();

        } else if (strings.size() > 0) {
            Collections.reverse(strings);
            myCreationAdapter = new CreationAdapter(getApplicationContext(), strings);
            rec_my_creation.setAdapter(myCreationAdapter);
            myCreationAdapter.setInterface(this);
            myCreationAdapter.notifyDataSetChanged();
        }


    }


    @Override
    public void ItemClick(View view, int pos) {
        Intent intent = new Intent(getApplicationContext(), FullScreenImageActivity.class);
        intent.putExtra("position", pos);
        startActivity(intent);
    }

    @Override
    public void ItemDeleteClick(int pos, String path) {
        showDeleteDialog(path);
    }


    @Override
    public void ItemEditClick(int pos, String path) {
        Log.e("imgpath==", "" + path);
        Intent intent = new Intent(getApplicationContext(), PaintActivity.class);
        intent.putExtra("imgPath", path);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        Runtime.getRuntime().gc();
        super.onDestroy();

    }

    // Delete Image Dialog
    public void showDeleteDialog(final String s) {
        AlertDialog.Builder builder = new AlertDialog.Builder(CreationActivity.this);
        builder.setTitle("Are you sure you want to delete this photo");
        builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                File file = new File(s);
                file.delete();
                Intent intent = new Intent(getApplicationContext(), CreationActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            }
        });
        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        try {
            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            startActivity(intent);
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            Log.e("outmemory==", "" + e.getMessage());
            finish();
        }
    }
}
