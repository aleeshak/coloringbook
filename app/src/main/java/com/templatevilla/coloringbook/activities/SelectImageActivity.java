package com.templatevilla.coloringbook.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.templatevilla.coloringbook.adapter.SelectImageAdapter;
import com.templatevilla.coloringbook.constant.Constant;
import com.templatevilla.coloringbook.utils.ConnectionDetector;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.templatevilla.coloringbook.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.templatevilla.coloringbook.constant.Constant.IMG_FOLDER;

/**
 * Created by Templatesvilla on 7/30/2018.
 *
 */

public class SelectImageActivity extends AppCompatActivity {

    public static List<String> getimglists = new ArrayList<>();
    @BindView(R.id.rec_main)
    RecyclerView rec_main;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    RecyclerView.LayoutManager layoutManager;
    SelectImageAdapter recMainAdapter;
    int get_rec_pos;
    AdView mAdView;
    int Mainposition = 0;
    boolean interstitialCanceled;
    InterstitialAd mInterstitialAd;
    ConnectionDetector cd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_image);
        ButterKnife.bind(this);
        get_rec_pos = getIntent().getIntExtra("imgPosition", 0);
        init();
        showbanner();
    }

    private void showbanner() {
        mAdView = findViewById(R.id.nativeadView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);
    }

    private void init() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        layoutManager = new GridLayoutManager(getApplicationContext(), 2);
        rec_main.setLayoutManager(layoutManager);
        rec_main.smoothScrollToPosition(get_rec_pos);

    }


    public void setMainAdapter() {
        getimglists.clear();
        getimglists = Constant.getAllImages(IMG_FOLDER, getApplicationContext());
        recMainAdapter = new SelectImageAdapter(getApplicationContext(), getimglists, new SelectImageAdapter.ClickInterface() {
            @Override
            public void recItemClick(View view, int i) {
                Mainposition = i;
                if (!interstitialCanceled) {
                    if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
                        mInterstitialAd.show();
                    } else {
                        ContinueIntent();
                    }
                }
            }
        });
        rec_main.setAdapter(recMainAdapter);
        recMainAdapter.notifyDataSetChanged();
    }


    @Override
    public void onBackPressed() {
        try {
            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            Log.e("outmemory==", "" + e.getMessage());
            finish();
        }

    }

    @Override
    protected void onDestroy() {
        Runtime.getRuntime().gc();
        super.onDestroy();

    }

    private void ContinueIntent() {
        try {
            Intent intent = new Intent(getApplicationContext(), PaintActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            intent.putExtra("pos", Mainposition);
            Log.e("pos==", "" + Mainposition);
            startActivity(intent);
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            Log.e("outmemory==", "" + e.getMessage());
            finish();

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        interstitialCanceled = false;
        if (getResources().getString(R.string.ADS_VISIBILITY).equals("YES")) {
            CallNewInsertial();
        }
        setMainAdapter();

    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequest);
    }

    private void CallNewInsertial() {
        cd = new ConnectionDetector(SelectImageActivity.this);
        if (cd.isConnectingToInternet()) {
            mInterstitialAd = new InterstitialAd(SelectImageActivity.this);
            mInterstitialAd.setAdUnitId(getString(R.string.admob_interstitial_id));
            requestNewInterstitial();
            mInterstitialAd.setAdListener(new AdListener() {
                public void onAdClosed() {
                    ContinueIntent();
                }
            });
        }
    }

    @Override
    public void onPause() {
        mInterstitialAd = null;
        interstitialCanceled = true;
        super.onPause();
    }
}
