package com.templatevilla.coloringbook.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.templatevilla.coloringbook.constant.Constant;
import com.templatevilla.coloringbook.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.templatevilla.coloringbook.constant.Constant.shareImg;

/**
 * Created by Templatesvilla on 7/30/2018.
 *
 */

public class FullScreenImageActivity extends AppCompatActivity {

    public static List<String> imgList1 = new ArrayList<>();
    int getPos;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.pager_creation)
    ViewPager pager_creation;
    List<String> getAllfile = new ArrayList<>();
    FullScreenAdapter fullScreenAdapter;
    String imgPath, imgPath1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen_creation);
        ButterKnife.bind(this);
        getPos = getIntent().getIntExtra("position", 0);
        Log.e("position1", "" + getPos);
        init();
        pager_creation.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                int gettag = pager_creation.getCurrentItem();
                Log.e("position2", "" + position);
                Log.e("position4", "" + gettag);
                imgPath1 = imgList1.get(position);
            }

            @Override
            public void onPageSelected(int position) {
                Log.e("position3", "" + position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void init() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        getAllfile = new ArrayList<>();
        getAllfile = Constant.getAllCreationList();
        Collections.reverse(getAllfile);
        if (!getAllfile.isEmpty() || getAllfile.size() != 0) {
            fullScreenAdapter = new FullScreenAdapter(getApplicationContext(), getAllfile);
            pager_creation.setAdapter(fullScreenAdapter);
            pager_creation.setCurrentItem(getPos);

        }

        pager_creation.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {


            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.share_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {

            case R.id.iv_share:
                Log.e("path", "" + imgPath1);
                shareImg(imgPath1, getApplicationContext());
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        try {
            Intent intent = new Intent(getApplicationContext(), CreationActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
        } catch (OutOfMemoryError e) {
            Log.e("outmemory==", "" + e.getMessage());
            e.printStackTrace();
        }
    }

    public class FullScreenAdapter extends PagerAdapter {

        Context context;

        ImageView img_full_screen;

        FullScreenAdapter(Context context, List<String> imgList) {
            this.context = context;
            imgList1 = imgList;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            View view = LayoutInflater.from(context).inflate(R.layout.ite_fullscreen_image, container, false);
            img_full_screen = view.findViewById(R.id.img_full_screen);
            Bitmap bitmap = BitmapFactory.decodeFile(imgList1.get(position));
            img_full_screen.setImageBitmap(bitmap);
            container.addView(view);
            return view;

        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return imgList1.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }
    }
}
