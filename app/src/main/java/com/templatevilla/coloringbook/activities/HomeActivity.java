package com.templatevilla.coloringbook.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.templatevilla.coloringbook.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Templatesvilla on 7/30/2018.
 *
 */


public class HomeActivity extends AppCompatActivity {

    @BindView(R.id.img_start)
    ImageView img_start;
    @BindView(R.id.img_mywork)
    ImageView img_mywork;
    @BindView(R.id.img_feedback)
    ImageView img_feedback;
    @BindView(R.id.img_rate)
    ImageView img_rate;
    PermissionActivity permissionActivity;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        ButterKnife.bind(this);
        permissionActivity = new PermissionActivity(this);

        if (!PermissionActivity.checkPermission()) {
            permissionActivity.requestPermission();
        }
        img_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectImageActivity.getimglists.clear();
                try {
                    Intent intent = new Intent(getApplicationContext(), SelectImageActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                    Log.e("outmemory==", "" + e.getMessage());

                }
            }
        });

        img_mywork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(getApplicationContext(), CreationActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                    Log.e("outmemory==", "" + e.getMessage());

                }
            }
        });

        img_rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String appPackageName = getPackageName();
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });

        img_feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendFeedback();
            }
        });

    }

    // Send FeedBack
    private void sendFeedback() {

        Intent localIntent = new Intent(Intent.ACTION_SEND);
        localIntent.putExtra(Intent.EXTRA_EMAIL, R.string.mail_feedback_email);
        localIntent.putExtra(Intent.EXTRA_CC, "");
        String str;
        try {
            str = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            localIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback for ColorBook");
            localIntent.putExtra(Intent.EXTRA_TEXT, "\n\n----------------------------------\n Device OS: Android \n Device OS version: " +
                    Build.VERSION.RELEASE + "\n App Version: " + str + "\n Device Brand: " + Build.BRAND +
                    "\n Device Model: " + Build.MODEL + "\n Device Manufacturer: " + Build.MANUFACTURER);
            localIntent.setType("message/rfc822");
            startActivity(Intent.createChooser(localIntent, "Choose an Email client :"));
        } catch (Exception e) {
            Log.d("OpenFeedback", e.getMessage());
        }
    }


    // Exit Dialog
    public void showExitDialog() {

        new AlertDialog.Builder(this)
                .setTitle("Closing Activity")
                .setMessage("Are you sure you want to close ?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        try {
                            HomeActivity.this.finishAffinity();
                        } catch (Exception e) {
                            e.printStackTrace();
                            finish();
                        }

                    }

                })
                .setNegativeButton("No", null)
                .show();
    }

    @SuppressLint("ObsoleteSdkInt")
    @Override
    public void onBackPressed() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            showExitDialog();
        }
    }
}
