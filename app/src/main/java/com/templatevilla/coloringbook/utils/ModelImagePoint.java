package com.templatevilla.coloringbook.utils;

import android.graphics.Point;

/**
 * Created by dream1_pc on 8/17/2018.
 */

public class ModelImagePoint {

    int x;
    int y;
    public int color;
    public Point point;

    public ModelImagePoint(int color, Point point) {
        this.color = color;
        this.point = point;
    }
//    public ModelImagePoint(int x, int y, int replace) {
//        this.x = x;
//        this.y = y;
//        this.color = replace;
//    }
}
