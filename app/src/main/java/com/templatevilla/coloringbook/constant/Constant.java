package com.templatevilla.coloringbook.constant;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.os.Build.VERSION_CODES.M;

/**
 * Created by Templatesvilla on 7/30/2018.
 *
 */

public class Constant {
    public static String SAVED_IMG_PATH = Environment.getExternalStorageDirectory() + "/ColorBook/";
    public static String IMG_FOLDER = "paintimage";
    private static List<Integer> strings = new ArrayList<>();

    public static boolean imgSavedOrNot(String img_name) {
        if (img_name != null) {
            File dir = new File(Constant.SAVED_IMG_PATH);
            if (dir.exists()) {
                File[] files = dir.listFiles();
                for (File file : files) {
                    if (img_name.equals(file.getAbsolutePath())) {
                        Log.e("match==", "true");
                        return true;
                    }
                }
            }
        }
        return false;

    }


    public static List<String> getAllCreationList() {
        List<String> filelist = new ArrayList<>();
        File dir = new File(SAVED_IMG_PATH);
        if (dir.exists()) {
            File[] files = dir.listFiles();
            for (File file : files) {
                filelist.add(file.getAbsolutePath());
            }
            return filelist;
        }
        return filelist;
    }

    public static List<String> getAllImages(String path, Context context) {
        String[] images = new String[0];
        try {
            images = context.getAssets().list(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ArrayList<String> listImages = new ArrayList<String>(Arrays.asList(images));
        return listImages;
    }

    public static void shareImg(String path, Context context) {
        File savefile = new File(path);
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("image/png");
        Uri uri;

        if (Build.VERSION.SDK_INT >= M) {
            uri = FileProvider.getUriForFile(context, context.getPackageName() + ".provider", savefile);
        } else {
            uri = Uri.fromFile(savefile);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName())));
        intent.putExtra(Intent.EXTRA_TEXT, Uri.parse("https://play.google.com/store/apps/details?id=") + context.getApplicationContext().getPackageName());
//        intent.putExtra(Intent.EXTRA_TEXT, context.getApplicationContext().getPackageName());
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        Intent chooserIntent = Intent.createChooser(intent, "Share Image");
//        chooserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(chooserIntent);
    }

}
