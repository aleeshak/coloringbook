package com.templatevilla.coloringbook.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.templatevilla.coloringbook.constant.Constant;
import com.templatevilla.coloringbook.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Templatesvilla on 7/30/2018.
 *
 */

public class SelectImageAdapter extends RecyclerView.Adapter<SelectImageAdapter.MyViewHolder> {
    private Context context;
    private List<String> imgList1;
    private ClickInterface interfcaeobj;

    public SelectImageAdapter(Context context, List<String> imgList, ClickInterface interfcaeobj) {
        this.context = context;
        this.imgList1 = imgList;
        this.interfcaeobj = interfcaeobj;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_select_image, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        String imagepath = "file:///android_asset/" + Constant.IMG_FOLDER + "/" + imgList1.get(position);
        Glide.with(context).load(imagepath).into(holder.img_main);

    }


    @Override
    public int getItemCount() {
        return imgList1.size();
    }

    public interface ClickInterface {
        void recItemClick(View view, int i);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.img_main)
        ImageView img_main;

        MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (interfcaeobj != null) {
                interfcaeobj.recItemClick(view, getAdapterPosition());
            }
        }
    }
}
