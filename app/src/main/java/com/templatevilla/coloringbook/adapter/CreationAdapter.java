package com.templatevilla.coloringbook.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.templatevilla.coloringbook.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Templatesvilla on 7/30/2018.
 *
 */


public class CreationAdapter extends RecyclerView.Adapter<CreationAdapter.MyViewHolder> {
    private Context context;
    private List<String> pathlist = new ArrayList<>();
    private RecClickInterface interfaceObj;

    public CreationAdapter(Context context, List<String> pathlist) {
        this.context = context;
        this.pathlist = pathlist;
    }

    public void setInterface(RecClickInterface anInterface) {
        this.interfaceObj = anInterface;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_creation, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        Drawable drawable = ContextCompat.getDrawable(context, R.drawable.ic_share_black_24dp);
        assert drawable != null;
        drawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Log.e("imgpath==", "" + pathlist.get(position));
        Bitmap bitmap = BitmapFactory.decodeFile(pathlist.get(position));
        holder.img_creation.setImageBitmap(bitmap);

        holder.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (interfaceObj != null) {
                    interfaceObj.ItemDeleteClick(position, pathlist.get(position));
                }

            }
        });
        holder.btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (interfaceObj != null) {
                    interfaceObj.ItemEditClick(position, pathlist.get(position));
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return pathlist.size();
    }

    public interface RecClickInterface {
        void ItemClick(View view, int pos);

        void ItemDeleteClick(int pos, String path);

        void ItemEditClick(int pos, String path);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.img_creation)
        ImageView img_creation;
        @BindView(R.id.btn_delete)
        ImageView btn_delete;
        @BindView(R.id.btn_edit)
        ImageView btn_edit;

        MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (interfaceObj != null) {
                interfaceObj.ItemClick(view, getAdapterPosition());
            }
        }
    }
}
