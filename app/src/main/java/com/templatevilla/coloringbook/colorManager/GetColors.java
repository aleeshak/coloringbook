package com.templatevilla.coloringbook.colorManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Templatesvilla on 7/30/2018.
 *
 */


public class GetColors {

    private static List<ColorUtil> colorUtils = new ArrayList<>();

    private static ColorUtil setColor1() {
        ColorUtil colorUtil = new ColorUtil();
        SetColor setColor = new SetColor();
        colorUtil.name = "Roller Rink";

        setColor.index = 0;
        setColor.color = "#29294b";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 1;
        setColor.color = "#F16A26";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 2;
        setColor.color = "#29565c";
        colorUtil.getSetColors().add(setColor);

        setColor = new SetColor();
        setColor.index = 3;
        setColor.color = "#F02640";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 4;
        setColor.color = "#00C482";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 5;
        setColor.color = "#A2275E";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 6;
        setColor.color = "#A8DB5A";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 7;
        setColor.color = "#721364";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 8;
        setColor.color = "#FBC034";
        colorUtil.getSetColors().add(setColor);


        return colorUtil;
    }

    private static ColorUtil setColor2() {
        ColorUtil colorUtil = new ColorUtil();
        SetColor setColor = new SetColor();
        colorUtil.name = "Vintage Sweater";

        setColor.index = 0;
        setColor.color = "#FED69B";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 1;
        setColor.color = "#FEBC80";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 2;
        setColor.color = "#F06441";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 3;
        setColor.color = "#FE8D51";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 4;
        setColor.color = "#D5C75A";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 5;
        setColor.color = "#9D9C7E";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 6;
        setColor.color = "#B60000";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 7;
        setColor.color = "#800000";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 8;
        setColor.color = "#5A475A";
        colorUtil.getSetColors().add(setColor);

        return colorUtil;
    }


    private static ColorUtil setColor3() {
        ColorUtil colorUtil = new ColorUtil();
        SetColor setColor = new SetColor();
        colorUtil.name = "Candy Store";

        setColor.index = 0;
        setColor.color = "#427D69";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 1;
        setColor.color = "#71C593";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 2;
        setColor.color = "#E9D888";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 3;
        setColor.color = "#5397C6";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 4;
        setColor.color = "#F3966B";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 5;
        setColor.color = "#C05068";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 6;
        setColor.color = "#9260B9";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 7;
        setColor.color = "#E577A4";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 8;
        setColor.color = "#E2588C";
        colorUtil.getSetColors().add(setColor);

        return colorUtil;
    }

    private static ColorUtil setColor4() {
        ColorUtil colorUtil = new ColorUtil();
        SetColor setColor = new SetColor();
        colorUtil.name = "Flower Basket";

        setColor.index = 0;
        setColor.color = "#FCE8C5";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 1;
        setColor.color = "#9ED6CB";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 2;
        setColor.color = "#FFFFDD";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 3;
        setColor.color = "#4D4F4E";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 4;
        setColor.color = "#FC9898";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 5;
        setColor.color = "#A3997E";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 6;
        setColor.color = "#FFDABD";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 7;
        setColor.color = "#5A475A";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 8;
        setColor.color = "#FFA800";
        colorUtil.getSetColors().add(setColor);

        return colorUtil;
    }

    private static ColorUtil setColor5() {
        ColorUtil colorUtil = new ColorUtil();
        SetColor setColor = new SetColor();
        colorUtil.name = "Humming Bird";

        setColor.index = 0;
        setColor.color = "#FCEBB7";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 1;
        setColor.color = "#68727B";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 2;
        setColor.color = "#F0A830";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 3;
        setColor.color = "#6991AB";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 4;
        setColor.color = "#F07819";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 5;
        setColor.color = "#78C0A8";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 6;
        setColor.color = "#D65C37";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 7;
        setColor.color = "#C3D7DE";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 8;
        setColor.color = "#5E412F";
        colorUtil.getSetColors().add(setColor);

        return colorUtil;
    }

    private static ColorUtil setColor6() {
        ColorUtil colorUtil = new ColorUtil();
        SetColor setColor = new SetColor();
        colorUtil.name = "Soda Pop";

        setColor.index = 0;
        setColor.color = "#E8B81A";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 1;
        setColor.color = "#28ABE3";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 2;
        setColor.color = "#FFA200";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 3;
        setColor.color = "#24A8AC";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 4;
        setColor.color = "#DB3340";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 5;
        setColor.color = "#20DA9B";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 6;
        setColor.color = "#982395";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 7;
        setColor.color = "#00A03E";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 8;
        setColor.color = "#0087CB";
        colorUtil.getSetColors().add(setColor);

        return colorUtil;
    }

    private static ColorUtil setColor7() {
        ColorUtil colorUtil = new ColorUtil();
        SetColor setColor = new SetColor();
        colorUtil.name = "Sunset";

        setColor.index = 0;
        setColor.color = "#69D2E7";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 1;
        setColor.color = "#FA6900";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 2;
        setColor.color = "#FDD059";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 3;
        setColor.color = "#BFE1C0";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 4;
        setColor.color = "#A1D7D9";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 5;
        setColor.color = "#DD5942";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 6;
        setColor.color = "#F48631";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 7;
        setColor.color = "#E0E4CD";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 8;
        setColor.color = "#A8DBDC";
        colorUtil.getSetColors().add(setColor);

        return colorUtil;
    }

    private static ColorUtil setColor8() {
        ColorUtil colorUtil = new ColorUtil();
        SetColor setColor = new SetColor();
        colorUtil.name = "Thrift Shop";

        setColor.index = 0;
        setColor.color = "#D79C8C";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 1;
        setColor.color = "#69A8AD";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 2;
        setColor.color = "#EF9950";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 3;
        setColor.color = "#28BE9B";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 4;
        setColor.color = "#F17D80";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 5;
        setColor.color = "#92DCDF";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 6;
        setColor.color = "#747496";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 7;
        setColor.color = "#C4D4AF";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 8;
        setColor.color = "#6D8671";
        colorUtil.getSetColors().add(setColor);

        return colorUtil;
    }

    private static ColorUtil setColor9() {
        ColorUtil colorUtil = new ColorUtil();
        SetColor setColor = new SetColor();
        colorUtil.name = "Rainbow";

        setColor.index = 0;
        setColor.color = "#BD0FE0";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 1;
        setColor.color = "#FEF200";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 2;
        setColor.color = "#4990E2";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 3;
        setColor.color = "#FFA800";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 4;
        setColor.color = "#4FE3C1";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 5;
        setColor.color = "#FB7105";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 6;
        setColor.color = "#B9E986";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 7;
        setColor.color = "#F62401";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 8;
        setColor.color = "#61F847";
        colorUtil.getSetColors().add(setColor);

        return colorUtil;
    }


    private static ColorUtil setColor10() {
        ColorUtil colorUtil = new ColorUtil();
        SetColor setColor = new SetColor();
        colorUtil.name = "Beach Time";

        setColor.index = 0;
        setColor.color = "#B44220";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 1;
        setColor.color = "#42B6DB";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 2;
        setColor.color = "#F2673E";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 3;
        setColor.color = "#408BCE";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 4;
        setColor.color = "#FFC652";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 5;
        setColor.color = "#94648E";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 6;
        setColor.color = "#CCC75F";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 7;
        setColor.color = "#7B4B75";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 8;
        setColor.color = "#78C8A3";
        colorUtil.getSetColors().add(setColor);

        return colorUtil;
    }

    private static ColorUtil setColor11() {
        ColorUtil colorUtil = new ColorUtil();
        SetColor setColor = new SetColor();
        colorUtil.name = "Tea Time";

        setColor.index = 0;
        setColor.color = "#E2D893";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 1;
        setColor.color = "#73503C";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 2;
        setColor.color = "#A79E65";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 3;
        setColor.color = "#591E22";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 4;
        setColor.color = "#73AFB7";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 5;
        setColor.color = "#2C2302";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 6;
        setColor.color = "#609DA0";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 7;
        setColor.color = "#D94D67";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 8;
        setColor.color = "#A68572";
        colorUtil.getSetColors().add(setColor);

        return colorUtil;
    }

    private static ColorUtil setColor12() {
        ColorUtil colorUtil = new ColorUtil();
        SetColor setColor = new SetColor();
        colorUtil.name = "Electric";

        setColor.index = 0;
        setColor.color = "#FAC8BF";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 1;
        setColor.color = "#698D9D";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 2;
        setColor.color = "#90C5A9";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 3;
        setColor.color = "#466675";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 4;
        setColor.color = "#F8E1B5";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 5;
        setColor.color = "#775CA3";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 6;
        setColor.color = "#F98A5F";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 7;
        setColor.color = "#442D65";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 8;
        setColor.color = "#CCCCCC";
        colorUtil.getSetColors().add(setColor);

        return colorUtil;
    }

    private static ColorUtil setColor13() {
        ColorUtil colorUtil = new ColorUtil();
        SetColor setColor = new SetColor();
        colorUtil.name = "Mellow Vibes";

        setColor.index = 0;
        setColor.color = "#8F9183";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 1;
        setColor.color = "#7B9EB1";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 2;
        setColor.color = "#E9D888";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 3;
        setColor.color = "#5397C6";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 4;
        setColor.color = "#F3966B";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 5;
        setColor.color = "#427D69";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 6;
        setColor.color = "#E2644E";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 7;
        setColor.color = "#515749";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 8;
        setColor.color = "#9AC085";
        colorUtil.getSetColors().add(setColor);

        return colorUtil;
    }

    private static ColorUtil setColor14() {
        ColorUtil colorUtil = new ColorUtil();
        SetColor setColor = new SetColor();
        colorUtil.name = "Mossy Stones";

        setColor.index = 0;
        setColor.color = "#F2FEDA";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 1;
        setColor.color = "#005657";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 2;
        setColor.color = "#CFEABF";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 3;
        setColor.color = "#004E7F";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 4;
        setColor.color = "#F7E6D2";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 5;
        setColor.color = "#004E99";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 6;
        setColor.color = "#F7BAB5";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 7;
        setColor.color = "#0061C4";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 8;
        setColor.color = "#AAD198";
        colorUtil.getSetColors().add(setColor);

        return colorUtil;
    }

    private static ColorUtil setColor15() {
        ColorUtil colorUtil = new ColorUtil();
        SetColor setColor = new SetColor();
        colorUtil.name = "Flower Bouquet";

        setColor.index = 0;
        setColor.color = "#FFE1B5";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 1;
        setColor.color = "#F8BEE7";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 2;
        setColor.color = "#FF9700";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 3;
        setColor.color = "#CF649A";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 4;
        setColor.color = "#FF7701";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 5;
        setColor.color = "#9C3167";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 6;
        setColor.color = "#CD2153";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 7;
        setColor.color = "#520936";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 8;
        setColor.color = "#ABA8FF";
        colorUtil.getSetColors().add(setColor);

        return colorUtil;
    }

    private static ColorUtil setColor16() {
        ColorUtil colorUtil = new ColorUtil();
        SetColor setColor = new SetColor();
        colorUtil.name = "Rainforest";

        setColor.index = 0;
        setColor.color = "#F86834";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 1;
        setColor.color = "#A68F59";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 2;
        setColor.color = "#F15E57";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 3;
        setColor.color = "#84BF17";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 4;
        setColor.color = "#483620";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 5;
        setColor.color = "#B8BE1C";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 6;
        setColor.color = "#363636";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 7;
        setColor.color = "#DCDDCD";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 8;
        setColor.color = "#83683B";
        colorUtil.getSetColors().add(setColor);

        return colorUtil;
    }

    private static ColorUtil setColor17() {
        ColorUtil colorUtil = new ColorUtil();
        SetColor setColor = new SetColor();
        colorUtil.name = "Succulent Garden";

        setColor.index = 0;
        setColor.color = "#EA4C6F";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 1;
        setColor.color = "#7EC2AB";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 2;
        setColor.color = "#AA2159";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 3;
        setColor.color = "#C6D6CC";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 4;
        setColor.color = "#56102C";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 5;
        setColor.color = "#FDF201";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 6;
        setColor.color = "#5A6962";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 7;
        setColor.color = "#BCC747";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 8;
        setColor.color = "#009C98";
        colorUtil.getSetColors().add(setColor);

        return colorUtil;
    }

    private static ColorUtil setColor18() {
        ColorUtil colorUtil = new ColorUtil();
        SetColor setColor = new SetColor();
        colorUtil.name = "Neon";

        setColor.index = 0;
        setColor.color = "#00C4EA";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 1;
        setColor.color = "#FF9B15";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 2;
        setColor.color = "#01E7CB";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 3;
        setColor.color = "#FF5057";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 4;
        setColor.color = "#5EFF63";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 5;
        setColor.color = "#FF00AA";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 6;
        setColor.color = "#00DE01";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 7;
        setColor.color = "#9C03FC";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 8;
        setColor.color = "#FFEA00";
        colorUtil.getSetColors().add(setColor);

        return colorUtil;
    }

    private static ColorUtil setColor19() {
        ColorUtil colorUtil = new ColorUtil();
        SetColor setColor = new SetColor();
        colorUtil.name = "Tree Line";

        setColor.index = 0;
        setColor.color = "#E0C279";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 1;
        setColor.color = "#95CE7D";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 2;
        setColor.color = "#C08120";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 3;
        setColor.color = "#3C972E";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 4;
        setColor.color = "#8E5100";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 5;
        setColor.color = "#006603";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 6;
        setColor.color = "#553101";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 7;
        setColor.color = "#003B01";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 8;
        setColor.color = "#CBEBC6";
        colorUtil.getSetColors().add(setColor);

        return colorUtil;
    }

    private static ColorUtil setColor20() {
        ColorUtil colorUtil = new ColorUtil();
        SetColor setColor = new SetColor();
        colorUtil.name = "Gelatin";

        setColor.index = 0;
        setColor.color = "#F9FBBA";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 1;
        setColor.color = "#FF432F";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 2;
        setColor.color = "#CCC51C";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 3;
        setColor.color = "#B92070";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 4;
        setColor.color = "#FEE600";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 5;
        setColor.color = "#0D2366";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 6;
        setColor.color = "#FEAC00";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 7;
        setColor.color = "#3A0C55";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 8;
        setColor.color = "#F05A28";
        colorUtil.getSetColors().add(setColor);

        return colorUtil;
    }

    private static ColorUtil setColor21() {
        ColorUtil colorUtil = new ColorUtil();
        SetColor setColor = new SetColor();
        colorUtil.name = "Oceanography";

        setColor.index = 0;
        setColor.color = "#F8FCEE";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 1;
        setColor.color = "#48B3D5";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 2;
        setColor.color = "#DFF3DA";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 3;
        setColor.color = "#238BC0";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 4;
        setColor.color = "#CCECC5";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 5;
        setColor.color = "#0066AE";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 6;
        setColor.color = "#A5DEB3";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 7;
        setColor.color = "#023E84";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 8;
        setColor.color = "#79CCC4";
        colorUtil.getSetColors().add(setColor);

        return colorUtil;
    }

    private static ColorUtil setColor22() {
        ColorUtil colorUtil = new ColorUtil();
        SetColor setColor = new SetColor();
        colorUtil.name = "Lily Pad";

        setColor.index = 0;
        setColor.color = "#FFFDE4";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 1;
        setColor.color = "#3CAC5A";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 2;
        setColor.color = "#F7FDB7";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 3;
        setColor.color = "#1C8540";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 4;
        setColor.color = "#D7F19B";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 5;
        setColor.color = "#016936";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 6;
        setColor.color = "#ACDE89";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 7;
        setColor.color = "#004729";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 8;
        setColor.color = "#75C775";
        colorUtil.getSetColors().add(setColor);

        return colorUtil;
    }

    private static ColorUtil setColor23() {
        ColorUtil colorUtil = new ColorUtil();
        SetColor setColor = new SetColor();
        colorUtil.name = "Black And White";

        setColor.index = 0;
        setColor.color = "#FFFFFF";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 1;
        setColor.color = "#B0B0B0";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 2;
        setColor.color = "#F3F3F3";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 3;
        setColor.color = "#878787";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 4;
        setColor.color = "#DADADA";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 5;
        setColor.color = "#5A5A5A";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 6;
        setColor.color = "#C3C3C3";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 7;
        setColor.color = "#2D2D2D";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 8;
        setColor.color = "#C9C9C9";
        colorUtil.getSetColors().add(setColor);

        return colorUtil;
    }


    private static ColorUtil setColor24() {
        ColorUtil colorUtil = new ColorUtil();
        SetColor setColor = new SetColor();
        colorUtil.name = "Skin Tone";

        setColor.index = 0;
        setColor.color = "#3C2F29";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 1;
        setColor.color = "#D1A18B";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 2;
        setColor.color = "#59453C";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 3;
        setColor.color = "#F0B8A1";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 4;
        setColor.color = "#785C50";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 5;
        setColor.color = "#FFCEB5";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 6;
        setColor.color = "#967264";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 7;
        setColor.color = "#FEE5C7";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 8;
        setColor.color = "#B48B79";
        colorUtil.getSetColors().add(setColor);

        return colorUtil;
    }

    private static ColorUtil setColor25() {
        ColorUtil colorUtil = new ColorUtil();
        SetColor setColor = new SetColor();
        colorUtil.name = "Metals";

        setColor.index = 0;
        setColor.color = "#E0C672";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 1;
        setColor.color = "#878F9C";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 2;
        setColor.color = "#BDA53B";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 3;
        setColor.color = "#827E75";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 4;
        setColor.color = "#AF856D";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 5;
        setColor.color = "#6C6747";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 6;
        setColor.color = "#AD6632";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 7;
        setColor.color = "#342F2B";
        colorUtil.getSetColors().add(setColor);
        setColor = new SetColor();
        setColor.index = 8;
        setColor.color = "#9D9D93";
        colorUtil.getSetColors().add(setColor);

        return colorUtil;
    }


    private static void setColorData() {
        colorUtils.add(setColor1());
        colorUtils.add(setColor2());
        colorUtils.add(setColor3());
        colorUtils.add(setColor4());
        colorUtils.add(setColor5());
        colorUtils.add(setColor6());
        colorUtils.add(setColor7());
        colorUtils.add(setColor8());
        colorUtils.add(setColor9());
        colorUtils.add(setColor10());
        colorUtils.add(setColor11());
        colorUtils.add(setColor12());
        colorUtils.add(setColor13());
        colorUtils.add(setColor14());
        colorUtils.add(setColor15());
        colorUtils.add(setColor16());
        colorUtils.add(setColor17());
        colorUtils.add(setColor18());
        colorUtils.add(setColor19());
        colorUtils.add(setColor20());
        colorUtils.add(setColor21());
        colorUtils.add(setColor22());
        colorUtils.add(setColor23());
        colorUtils.add(setColor24());
        colorUtils.add(setColor25());
    }

    public static List<ColorUtil> getallcolordata() {
        setColorData();
        return colorUtils;
    }
}
